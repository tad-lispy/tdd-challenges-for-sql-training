#+title: Lesson plan


* Basic concepts

** What is SQL?

It has something to do with databases.

Databases have to do with data (duh!)

Data is an ingredient required to answer questions. The other ingredient is skill (aka logic or computation).


** What is a question? How meta!

Consider the following:

- What is 2 + 2?
- What is your name?
- How many years ago did the king of the Netherlands go to primary school?

*** Where does the data come from?

The first one contains all the data you need in the question itself. All you need is skill.

Hopefully you know the answer to the second one. The data is in you.

Third one is a tough cookie. It requires skill to ask sub-questions:

- Who is the king of the Netherlands?

  If you don't know, Wikipedia can help (external data).

- What year did he graduate?

  Probably Wikipedia again.

- What year is now?

  Hopefully you know. Else ask your doctor.

- How much is now - then?

  Mad math skills.


** Why do we need SQL

Computers are mostly for answering questions, like:

- Which pictures of cats are the most cute right now?
- Shall the graphite rods be lowered into the reactor?
- Does the username and password match?


*** Persistence
*** Single source of truth for distributed systems
*** Standardized operations

Looking at it from a high level, most applications perform a limited set of operations:

- Store data
- Read stored data
- Update what was previously stored
- Combine data
- Aggregate results
- Delete obsolete data

SQL provides a standardized language to perform those common operations.




** Nuff philosophy! Introduce MySQL Workbench

*** Close stupid secondary sidebar

*** Try if it works

#+begin_src sql
Select 2 + 2 as four;
#+end_src


** Basic structures

*** Entities and relations

For example a person is an entity.
A house is another one.
A person can own a house (relation).
A person can know another person (another relation).

*** Different entities have different properties

for example

- a person has a name, birthdate, level of education and a government issued id
- a house has an address, year of construction, number of floors, etc.

*** SQL databases store entities and their relations in tables

It offers a language (called SQL) to model the entities and relations, store information about them, extract it, etc.

Let's model some people

#+begin_src sqlite
Create table person (
  name text,
  birthdate real,
  education text,
  id text primary key
);

Insert into person
values
  (
    "Willem-Alexander",
    julianday("1967-04-27"),
    "doctorandus",
    "the-king-has-no-BSN"
  ), (
    "Tad Lispy",
    julianday("1989-03-22"),
    "streetwise",
    "123-not-really-my-id"
  )
;

Select
    rowid,
    name,
    date(birthdate) as birthdate,
    education,
    id
from person;
#+end_src









* Questions and assumptions

Read problems at https://tad-lispy.gitlab.io/sql-training/.

What data structures do we need to answer those questions?


* Labs labs labs!

Import sample data.

Loop:

In groups of 5 take 10 minutes to solve next question.

Shout the answer at me. I'm your typist for 7 minutes.

If not solved enable the_brain.

Goto Loop


* Homework

Get a basic understanding of

- many to many relations
- union
- cross join
- derived queries
