{
  description = "SQL training materials";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    extra-container.url = "github:erikarvstedt/extra-container";
  };

  outputs = { self, nixpkgs, extra-container, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      packages.${system}.default = extra-container.lib.buildContainers {
        inherit system;
        inherit nixpkgs;

        config = {
          containers.mysql = {
            extra.addressPrefix = "10.250.0";

            config = {
              services.mysql.enable = true;
              services.mysql.package = pkgs.mysql80;
              networking.firewall.allowedTCPPorts = [ 3306 ];
              systemd.services.mysql-grant-access = {
                after = [ "mysql.service" ];
                wantedBy = ["multi-user.target"];
                description = "Grant access to root account of MySQL server from all machines.";
                script = ''
                  echo 'Create user if not exists "root";' | ${pkgs.mysql}/bin/mysql
                  echo 'Grant all privileges on *.* to "root" with grant option;' | ${pkgs.mysql}/bin/mysql
                '';
                serviceConfig = {
                  Type = "oneshot";
                  RemainAfterExit = true;
                };
              };
            };
          };
        };
      };
      devShells.${system}.default = pkgs.mkShell {
        name = "sql-training-development-shell";
        packages = with pkgs; [
          pkgs.extra-container
          mysql80
          mysql-shell
          mysql-workbench
        ];
      };
    };
}
